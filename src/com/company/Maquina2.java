package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Maquina2 {
    ArrayList<Slots> valoresSorteados = new ArrayList<>();
    int totalSoma = 0;
    boolean ehIgual = true;

    public void sortear() {
        Random random = new Random();

        for (int i = 0; i < 3; i++) {
            int numAleatorio = random.nextInt(Slots.values().length);
            valoresSorteados.add(Slots.values()[numAleatorio]);

            totalSoma = totalSoma + Slots.values()[numAleatorio].getValor();
        }

        somaBonus();
    }

    public void somaBonus(){
        boolean isBonus = valoresSorteados.stream().limit(valoresSorteados.size()).distinct().count() == 1;

        if (isBonus) {
            totalSoma = totalSoma * 100;
        }
    }

    public void imprimirSorteio(){
        System.out.println(valoresSorteados);
        System.out.println(totalSoma);
    }

}

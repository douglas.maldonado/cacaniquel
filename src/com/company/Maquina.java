package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Maquina {
    ArrayList<Slots> slotLista = new ArrayList<>();
    ArrayList<String> valorSlot = new ArrayList<>();
    int somaValorSlot;

    public void sortearValor(){
        Random random = new Random();

        for (Slots p: Slots.values()){
            slotLista.add(p);
        }

        for(int i = 0; i < 3; i++){
            valorSlot.add(String.valueOf(slotLista.get(random.nextInt(Slots.values().length))));
        }

    }

    public void somarValor(){
        for  ( String vlSlot : valorSlot) {
            somaValorSlot = somaValorSlot + Slots.valueOf(vlSlot).valor;
        }
    }

    public void imprimirSlots(){
        for  ( String vlSlot : valorSlot) {
            System.out.print(vlSlot + " ");
        }
        System.out.println(somaValorSlot);
    }

}
